# markdown-menu-generator

A menu generator for Markdown document. It can be integrated with CI.

- [How to use it](#how-to-use-it)
	- [Get help](#get-help)
	- [Exemple of menu generation](#exemple-of-menu-generation)

## How to use it

Open command line terminal and call markdown-generator.py with desired args.

### Get help

python markdown-generator.py -help

### Exemple of menu generation

python markdown-generator.py -input_file "my-test.md" > "generated-menu.md"