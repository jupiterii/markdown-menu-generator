import sys
import argparse
import re


def create_menu(path):
    str_menu = ''
    lines = get_lines(path)

    for line in lines:
        lm = get_line_menu(line)
        if lm:
            level = lm[0]
            title = lm[1]
            link = get_title_link(title)
            str_menu += ''.join(['\t' for _ in range(level - 1)])
            str_menu += f'- [{title}](#{link})\n'

    return str_menu

def get_line_menu(line):

    if not line.startswith('#'):
        return
    
    for i in range(6,0,-1):
        s = ''.join(['#' for _ in range(i)])
        if line.startswith(s):
            return (i, line[i:].strip())

def get_lines(path, encoding='utf-8'):
    with open(path, encoding=encoding) as f:
        lines = f.readlines()
        return [line.strip() for line in lines]

def get_title_link(title):
    clean_title1 = title.strip().lower()
    clean_title2 = clean_title1.replace("'", '')
    clean_title3 = re.sub('\?|\!', ' ', clean_title2)
    clean_title4 = re.sub('\s+|\t+', '-', clean_title3)
    return clean_title4


parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('-input_file')
parser.add_argument('-output_file')
parser.add_argument('-input_encoding')
parser.add_argument('-output_encoding')
parser.add_argument('-style')
parser.add_argument('-merge')

args = parser.parse_args()
my_menu = create_menu(args.input_file)

print(my_menu)

#with open(args.output_file, mode='w', encoding='utf-8') as f:
#    f.write(my_menu)
